.. PolyRound documentation master file, created by
   sphinx-quickstart on Wed Apr 26 14:26:52 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PolyRound's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   autoapi/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
