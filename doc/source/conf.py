# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.getcwd())))
from PolyRound.version import VERSION

project = "PolyRound"
copyright = "2023, Axel Theorell"
author = "Axel Theorell"
release = VERSION

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["autoapi.extension"]

templates_path = ["_templates"]
exclude_patterns = []
autoapi_template_dir = "_templates/autoapi"
language = "python"

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"
html_static_path = ["_static"]
autoapi_dirs = ["../../PolyRound"]
autoapi_exclude = ["../../PolyRound/unit_tests.py"]
autoapi_type = "python"


def skip_test_modules(app, what, name, obj, skip, options):
    if "unit_tests" in name:
        skip = True
    return skip


def setup(sphinx):
    sphinx.connect("autoapi-skip-member", skip_test_modules)
